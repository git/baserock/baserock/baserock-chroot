all: nothing

nothing:
	@echo "Nothing to see here"

pkg:
	dpkg-buildpackage -rfakeroot -uc -us -b

PREFIX ?= /usr

install:
	install -d $(DESTDIR)$(PREFIX)/sbin
	for TOOL in br-ct-sync-chroots enter-baserock manage-baserock; do \
		install -m 0755 $${TOOL} $(DESTDIR)$(PREFIX)/sbin/$${TOOL}; \
	done
	install -d $(DESTDIR)/opt/baserock/chroots
	install -d $(DESTDIR)/opt/baserock/src

