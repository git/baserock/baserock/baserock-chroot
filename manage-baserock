#!/bin/bash

# manage-baserock -- Tool to manage (add/list/remove etc) Baserock chroots

# Copyright (C) 2014  Codethink Ltd
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

BASEROCK_BASE="${BASEROCK_BASE:-/opt/baserock/chroots}"

ACTION="$1"
SYSTEM="$2"
URL="$3"

check_system_name () {
    local system="$1"
    local valid_chars='A-Za-z0-9_-.'
    case "$system" in
    *[^${valid_chars}]*)
        echo "The specified name '${system}' contains invalid characters."
        echo "Valid characters are ${valid_chars}."
        exit 1
    esac
}

list_baserocks () {
    (cd "${BASEROCK_BASE}" && ls -d */baserock) 2>/dev/null | sed -e's@/baserock$@@'
}

list_sessions_for_system () {
    local system="$1"
    schroot --list --all-sessions | grep "^session:baserock-${system}"
}

case "${ACTION}" in
    list)
        for SYSTEM in $(list_baserocks); do
            if test -e "${BASEROCK_BASE}/${SYSTEM}/default"; then
                echo "${SYSTEM} *"
            else
                echo "${SYSTEM}"
            fi
        done
        ;;
    add)
        if test $(id -u) != "0"; then
            exec sudo "$0" "$@"
        fi

        check_system_name "$SYSTEM"

        if test -d "${BASEROCK_BASE}/${SYSTEM}/baserock"; then
            echo "Sorry, $SYSTEM already exists"
            exit 1
        fi

        if test -z "$URL"; then
            echo "Usage: $0 add <name> <url>"
            exit 1
        fi

        mkdir -p "${BASEROCK_BASE}"

        TEMP_BASE=$(mktemp -d --tmpdir="${BASEROCK_BASE}")
        cleanup () { rm -rf "${TEMP_BASE}"; }
        trap cleanup 0

        if test "x${URL}" = "x${URL##http://}" -a "x${URL}" = "x${URL##https://}"; then
            if ! test -e "${URL}"; then
                echo "${URL} is neither a HTTP/HTTPS url nor a path to a local file";
                exit 1;
            fi
            if ! ln -s "$(readlink -e "${URL}")" "${TEMP_BASE}/tarball"; then
                echo "Sorry, could not link ${URL} into place";
                exit 1;
            fi
        else
            if ! wget -O "${TEMP_BASE}/tarball" "$URL"; then
                echo "Sorry, could not download ${URL}";
                exit 1;
            fi
        fi

        mkdir "${TEMP_BASE}/root"
        if ! tar -C "${TEMP_BASE}/root" -xf "${TEMP_BASE}/tarball"; then
            echo "Sorry, could not unpack ${URL}";
            exit 1;
        fi

        ENTRIES=$(ls "${TEMP_BASE}/root" | wc -l | sed -e's/^ *//;s/ *$//')
        if test "x${ENTRIES}" = "x1"; then
            mv "${TEMP_BASE}/root/"* "${BASEROCK_BASE}/${SYSTEM}"
        else
            mv "${TEMP_BASE}/root" "${BASEROCK_BASE}/${SYSTEM}"
        fi

        if ! test -d "${BASEROCK_BASE}/${SYSTEM}/baserock"; then
            echo "Sorry, ${URL} does not appear to contain a baserock system";
            rm -rf "${BASEROCK_BASE}/${SYSTEM}";
            exit 1;
        fi

        NR_BRS=$(list_baserocks | wc -l | sed -e's/^ *//;s/ *$//')
        if test "$NR_BRS" = "1"; then
            touch "${BASEROCK_BASE}/${SYSTEM}/default"
        fi

        if ! test -r "${BASEROCK_BASE}/${SYSTEM}/root/.bashrc"; then
            cat > "${BASEROCK_BASE}/${SYSTEM}/root/.bashrc" <<EOF
PS1="\u@\h(baserock-${SYSTEM}):\w\$ "
EOF
        fi

        if ! test -r "${BASEROCK_BASE}/${SYSTEM}/root/.bash_profile"; then
            cat > "${BASEROCK_BASE}/${SYSTEM}/root/.bash_profile" <<EOF
. ~/.bashrc
EOF
        fi

        if ! test -d "${BASEROCK_BASE}/${SYSTEM}/src"; then
            mkdir "${BASEROCK_BASE}/${SYSTEM}/src"
        fi

        br-ct-sync-chroots

        ;;
    rm)
        if test $(id -u) != "0"; then
            exec sudo "$0" "$@"
        fi

        if ! test -d "${BASEROCK_BASE}/${SYSTEM}/baserock"; then
            echo "Sorry, $SYSTEM does not exist or is not a baserock chroot"
            exit 1
        fi

        sessions="$(list_sessions_for_system $SYSTEM)"
        if ! test -z $sessions; then
            echo "I found the following active sessions for chroot '$SYSTEM':"
            echo
            echo "$sessions"
            echo
            echo "You might lose data in mounted filesystems if I were to"
            echo "recursively delete path ${BASEROCK_BASE}/${SYSTEM}."
            echo
            echo "If you think these sessions are stale, you can run the "
            echo "following command to end ALL schroot sessions:"
            echo
            echo "    schroot --end-session --all-sessions"
            exit 1
        fi

        rm -rf "${BASEROCK_BASE}/${SYSTEM}"
        br-ct-sync-chroots
        ;;
    set-default)
        if test $(id -u) != "0"; then
            exec sudo "$0" "$@"
        fi

        if ! test -d "${BASEROCK_BASE}/${SYSTEM}/baserock"; then
            echo "Sorry, $SYSTEM does not exist or is not a baserock chroot"
            exit 1
        fi
        
        rm -f "${BASEROCK_BASE}/"*/default
        touch "${BASEROCK_BASE}/${SYSTEM}/default"
        br-ct-sync-chroots
        ;;
    *)
        echo >&2 "usage: $0 list | add <name> <url> | rm <name> | set-default <name>"
        exit 1
        ;;
esac
