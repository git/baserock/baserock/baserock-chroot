#!/bin/bash

# br-ct-sync-chroots -- Synchronise Baserock chroots and schroot configuration

# Copyright (C) 2014  Codethink Ltd
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

ACTION="$1"

SCHROOT_BASE="${SCHROOT_BASE:-/etc/schroot}"
SCHROOT_CONF="${SCHROOT_CONF:-${SCHROOT_BASE}/schroot.conf}"
BASEROCK_BASE="${BASEROCK_BASE:-/opt/baserock/chroots}"
SRC_BASE="${SRC_BASE:-/opt/baserock/src}"

clear_entries () {
    sed -e'/BASEROCK_ENTRIES_BEGIN/,/BASEROCK_ENTRIES_END/d' -i "${SCHROOT_CONF}"
}

find_default () {
    grep "^aliases" "$SCHROOT_CONF" | grep -q "default"
}

add_conf_line () {
    echo "$@" >> "${SCHROOT_CONF}"
}

list_baserocks () {
    (cd "${BASEROCK_BASE}" && ls -d */baserock) 2>/dev/null | sed -e's@/baserock$@@'
}

add_entries () {
    add_conf_line "# BASEROCK_ENTRIES_BEGIN"
    while read entry; do
        add_conf_line "[baserock-$entry]"
        add_conf_line "type=directory"
        add_conf_line "directory=${BASEROCK_BASE}/${entry}"
        add_conf_line "description=Baserock in $entry"
        add_conf_line "users=root"
        add_conf_line "profile=baserock-$entry"
        if ! find_default; then
            test -e "${BASEROCK_BASE}/$entry/default" && add_conf_line "aliases=default"
        fi
    done < <(list_baserocks)
    add_conf_line "# BASEROCK_ENTRIES_END"
}

find_configs () {
    (cd "${SCHROOT_BASE}"; ls -d baserock-*/config) 2>/dev/null | sed -e's@/config$@@'
}

find_used_configs () {
    grep "^profile" "${SCHROOT_CONF}" | grep baserock | sed -e's at profile *= *@@'
}

is_config_used () {
    grep "profile *= *" "${SCHROOT_CONF}" | grep -q -F "$1"
}

add_new_configs () {
    for entry in $(list_baserocks); do
        if ! test -e "${SCHROOT_BASE}/baserock-${entry}/config"; then
            mkdir -p "${SCHROOT_BASE}/baserock-${entry}"
            cat > "${SCHROOT_BASE}/baserock-${entry}/config" <<EOF
FSTAB="${SCHROOT_BASE}/baserock-${entry}/fstab"
COPYFILES="${SCHROOT_BASE}/baserock-${entry}/copyfiles"
NSSDATABASES="${SCHROOT_BASE}/baserock-${entry}/nssdatabases"
EOF
            cat > "${SCHROOT_BASE}/baserock-${entry}/fstab" <<EOF
/proc           /proc           none    rw,bind        0       0
/sys            /sys            none    rw,bind        0       0
/dev            /dev            none    rw,bind         0       0
/dev/pts        /dev/pts        none    rw,bind         0       0
/tmp            /tmp            none    rw,bind         0       0
${SRC_BASE}     /src            none    rw,bind         0       0
EOF
            cat > "${SCHROOT_BASE}/baserock-${entry}/copyfiles" <<'EOF'
/etc/resolv.conf
EOF
            cat > "${SCHROOT_BASE}/baserock-${entry}/nssdatabases" <<'EOF'
networks
hosts
EOF
        fi
    done
}

clear_unused_configs () {
    while read config; do
        if ! is_config_used "$config"; then
            rm -rf "${SCHROOT_BASE}/${config}"
        fi
    done < <(find_configs)
}

case "$ACTION" in
    purge)
        clear_entries
        clear_unused_configs
        ;;
    *)
        clear_entries
        add_entries
        add_new_configs
        clear_unused_configs
        ;;
esac
